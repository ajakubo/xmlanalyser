package utils

import scala.util.parsing.input.Reader
import scala.util.parsing.input.Position

class SeqReader[T](seq: Seq[T], position: Int = 0) extends Reader[T] {

  lazy val _rest = new SeqReader(seq.tail, position + 1)

  def atEnd = seq.isEmpty
  def first = seq.head
  def rest = _rest

  def pos = new Position { def line = position; def column = 0; def lineContents = first.toString }
}