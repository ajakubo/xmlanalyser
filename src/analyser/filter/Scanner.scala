package analyser.filter

import scala.Array.canBuildFrom

object Scanner {

  def tokenStream(chars: Stream[Char]): Stream[Token] = {
    if(chars.isEmpty) return Stream.Empty
    tokenStream0(chars)
  }

  private def tokenStream0(chars: Stream[Char]): Stream[Token] = {
    val (token, rest) = processStream(chars, 0, "")
    token #:: tokenStream(rest)
  }

  private def processStream(chars: Stream[Char], state: Int, buffer: String): (Token, Stream[Char]) = {
    val c = chars.head
    state match {
      case 0 => c match {
        case '[' => (StartFilter, chars.tail)
        case ']' => (EndFilter, chars.tail)
        case '>' => (RelationPointer, chars.tail)
        case l if l.isLetterOrDigit => processStream(chars.tail, 1, buffer + l)
        case ws if ws.isWhitespace => (WhiteSpace, chars.tail)
      }
      case 1 => c match {
        case l if l.isLetterOrDigit => processStream(chars.tail, 1, buffer + l)
        case _ => (Word(buffer), chars)
      }
    }
  }
}