package analyser

import utils.SeqReader
import analyser.filter.FilterParser.{Success,NoSuccess}

package object filter { 
  def string2filter(filterInput: Stream[Char]): List[String] = {
    val filterTokens = Scanner.tokenStream(filterInput)
    val filterReader = new SeqReader(filterTokens)
    
    FilterParser.TrimmedFilterP(filterReader) match {
      case Success(r, _) => r
      case NoSuccess(msg, next) => throw new Exception("FILTER: Failed at line %s, column %s: %s".format(
        next.pos.line, next.pos.column, msg))
    }
  }
}