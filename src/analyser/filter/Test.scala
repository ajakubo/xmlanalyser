package analyser.filter

object Test extends App {
 
	val charStream: Stream[Char] = "[node>child>was>xDD>lolwat]".toStream
	val tokenStream = Scanner.tokenStream(charStream)
	
	tokenStream foreach println
	
}