package analyser.filter

import scala.util.parsing.combinator.Parsers

object FilterParser extends Parsers{
  type Elem = Token

  private def StartFilterP = elem(StartFilter)
  private def EndFilterP = elem(EndFilter)
  private def RelationPointerP = elem(RelationPointer)
  private def WhiteSpaceP = elem(WhiteSpace)
  private def WordP = elem("Word", _.isInstanceOf[Word]) ^^ (w => w.asInstanceOf[Word].value)
  
  private def Trimmed[T](parser: Parser[T]): Parser[T] = WhiteSpaceP.* ~ parser ~ WhiteSpaceP.* ^^ (_._1._2)
  
  private def TrimmedWordP = Trimmed(WordP) 
  
  private def FilterP = StartFilter ~ (TrimmedWordP ~ RelationPointerP).* ~ TrimmedWordP ~ EndFilterP ^^
  	(f =>  f._1._1._2.map( e => e._1 ) :+ f._1._2 )

  def TrimmedFilterP = Trimmed(FilterP)
}