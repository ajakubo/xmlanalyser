package analyser.filter

sealed abstract class Token

case object StartFilter extends Token		//	[
case object EndFilter extends Token			//	]
case object RelationPointer extends Token			//	>
case object WhiteSpace extends Token
case class Word(val value: String) extends Token

case object EndOfStream extends Token

