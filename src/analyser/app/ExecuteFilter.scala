package analyser.app

import analyser.xml.MyXmlElem
import analyser.xml.MyXmlText
import analyser.xml.MyXmlNode

object ExecuteFilter {

  def apply(xml: MyXmlElem, filters: List[String]): Option[MyXmlElem] = {
    filter(xml, filters, filters)
  }

  private def filter(xml: MyXmlElem, filterOrg: List[String], filterLocal: List[String]): Option[MyXmlElem] = {
    filterLocal match {
      case xml.name :: Nil => return None
      case xml.name :: _ => {
        val newChildren = xml.children collect {
          case child @ MyXmlElem(name, attributes, children) => filter(child, filterOrg, filterLocal.tail)
          case child @ MyXmlText(value) => Some(child)
        } flatten;
        
        Some(MyXmlElem(xml.name, xml.attributes, newChildren))
      }
      case _ :: _ => {
        val newChildren = xml.children collect {
          case child @ MyXmlElem(name, attributes, children) => filter(child, filterOrg, filterOrg)
          case child @ MyXmlText(value) => Some(child)
        } flatten;
        
        Some(MyXmlElem(xml.name, xml.attributes, newChildren))
      }
    }
  }

}

