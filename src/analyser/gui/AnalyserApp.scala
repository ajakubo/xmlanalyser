package analyser.gui

import scala.swing.SimpleSwingApplication
import analyser.app.ExecuteFilter
import analyser.filter.string2filter
import scala.swing.GridBagPanel
import scala.swing.MainFrame
import scala.swing.Label
import scala.swing.Swing
import scala.swing.ToggleButton
import scala.swing.Button
import scala.swing.CheckBox
import scala.swing.TextField
import scala.swing.ScrollPane
import scala.swing.TextArea
import javax.swing.border.Border
import scala.swing.event.ButtonClicked
import scala.swing.ScrollBar

object AnalyserApp extends SimpleSwingApplication {
  
  def top = new MainFrame {
    title = "TKOM - analizator XML";

    val filterButton = new Button("Filtruj")
    val xmlTextArea = new TextArea("XML", 40, 25)
    val filterTextArea = new TextArea("Filtry", 40, 25)
    val outputTextArea = new TextArea("Output", 40, 25)
    
    def execute {
      try {
        import analyser.xml._
        import analyser.filter._
        import analyser.app._
        
        val xml = string2xml(xmlTextArea.text.toStream)
        val filter = string2filter(filterTextArea.text.toStream)
        
        val filtered = ExecuteFilter(xml, filter).get
        
        outputTextArea.text = filtered.prettyPrint
        
      } catch {
        case e: Throwable => outputTextArea.text = e.getMessage()
      }
    }

    contents = new GridBagPanel {
      def constraints(x: Int, y: Int,
        gridwidth: Int = 1, gridheight: Int = 1,
        weightx: Double = 0.0, weighty: Double = 0.0,
        fill: GridBagPanel.Fill.Value = GridBagPanel.Fill.None): Constraints = {
        val c = new Constraints
        c.gridx = x
        c.gridy = y
        c.gridwidth = gridwidth
        c.gridheight = gridheight
        c.weightx = weightx
        c.weighty = weighty
        c.fill = fill
        c
      }
      
      add(filterButton, constraints(1, 0))
      
      add( new ScrollPane( xmlTextArea), constraints(0, 1, weightx = 1, fill = GridBagPanel.Fill.Both))
      add( new ScrollPane( filterTextArea), constraints(1, 1, weightx = 1, fill = GridBagPanel.Fill.Both))
      add( new ScrollPane( outputTextArea), constraints(2, 1, weightx = 1, fill = GridBagPanel.Fill.Both))
      
      listenTo(filterButton)
      reactions += {
        case ButtonClicked(_) => execute
      }
    }
  }

}