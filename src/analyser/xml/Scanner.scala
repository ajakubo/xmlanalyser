package analyser.xml

import scala.Array.canBuildFrom

object Scanner {

  def tokenStream(chars: Stream[Char]): Stream[Token] = {
    if (chars.isEmpty) return Stream.Empty
    tokenStream0(chars)
  }

  private def tokenStream0(chars: Stream[Char]): Stream[Token] = {
    val (token, rest) = processStream(chars, 0, "")
    token #:: tokenStream(rest)
  }

  private def processStream(chars: Stream[Char], state:Int, buffer: String): (Token, Stream[Char]) = {
    val c = chars.head
    state match {
      case 0 => c match {
        case '<' => processStream(chars.tail, 1, buffer)
        case '>' => (EndTag, chars.tail)
        case '=' => processStream(chars.tail, 2, buffer)
        case '"' => (EndAttribute, chars.tail)
        case l if l.isLetterOrDigit => processStream(chars.tail, 3, buffer + l)
        case ws if ws.isWhitespace => (WhiteSpace(ws), chars.tail)
      }
      case 1 => c match {
        case '/' => (StartClosingTag, chars.tail)
        case _ => (StartOpeningTag, chars)
      }
      case 2 => c match {
        case '"' => (StartAttribute, chars.tail)
      }
      case 3 => c match {
        case l if l.isLetterOrDigit => processStream(chars.tail, 3, buffer + l)
        case _ => (Word(buffer), chars)
      }
    }
  }
}