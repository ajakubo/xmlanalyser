package analyser.xml

sealed abstract class Token

case object StartOpeningTag extends Token	//	<
case object StartClosingTag extends Token	//	</
case object EndTag extends Token			//	>
case object StartAttribute extends Token 	//  ="
case object EndAttribute extends Token		//	"
case class Word(val value: String) extends Token
case class WhiteSpace(val value: Char) extends Token

