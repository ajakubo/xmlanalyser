package analyser.xml

sealed abstract class MyXmlNode {
  def prettyPrint() : String
}

case class MyXmlElem(name: String, attributes: List[MyXmlAttribute], children: List[MyXmlNode]) extends MyXmlNode{
  def this(name: String, attributes: List[MyXmlAttribute]) = this(name, attributes, Nil)
  override def prettyPrint() = {
    "<" + name + " " + attributes.map(_.prettyPrint()).mkString(" ") + ">" + children.map(_.prettyPrint()).mkString(" ") + "</" + name + ">"
  }
}
case class MyXmlText(value: String) extends MyXmlNode{
  override def prettyPrint() = value
}

case class MyXmlAttribute(name: String, value: String){
  def prettyPrint() = {
    name + "=\"" + value + "\""
  }
}