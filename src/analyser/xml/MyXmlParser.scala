package analyser.xml

import scala.util.parsing.combinator.Parsers

object MyXmlParser extends Parsers {
  type Elem = Token

  private def StartOpeningTagP = elem(StartOpeningTag)
  private def StartClosingTagP = elem(StartClosingTag)
  private def EndTagP = elem(EndTag)
  private def StartAttributeP = elem(StartAttribute)
  private def EndAttributeP = elem(EndAttribute)
  private def WordP = elem("Word", _.isInstanceOf[Word]) ^^ (w => w.asInstanceOf[Word].value)
  private def WhiteSpaceP = elem("WhiteSpace", _.isInstanceOf[WhiteSpace]) ^^ (ws => ws.asInstanceOf[WhiteSpace].value)

  private def TextP = WhiteSpaceP.* ~ WordP ~ (WhiteSpaceP.+ ~ WordP).* ~ WhiteSpaceP.* ^^
    (s => s._1._1._1.mkString("") + s._1._1._2 + s._1._2.foldLeft("")((acc, b) => acc + b._1.mkString("") + b._2) + s._2.mkString(""))

  private def XmlTextP = TextP ^^ (s => List(MyXmlText(s)))

  private def AttributeP = WordP ~ StartAttributeP ~ TextP ~ EndAttributeP ^^
    (a => MyXmlAttribute(a._1._1._1, a._1._2))

  private def TrimmedWordP = Trimmed(WordP)
  private def TrimmedAttributeP = Trimmed(AttributeP)

  private def EmptyNodeP = WhiteSpaceP.* ^^^ Nil

  private def Trimmed[T](parser: Parser[T]): Parser[T] = WhiteSpaceP.* ~ parser ~ WhiteSpaceP.* ^^ (_._1._2)

  private def XmlContentParser = (TrimmedElemP.* ||| XmlTextP ||| EmptyNodeP)

  def ElemP: Parser[MyXmlElem] = StartOpeningTagP ~ TrimmedWordP ~ TrimmedAttributeP.* ~
    EndTagP ~ XmlContentParser ~ StartClosingTagP ~ TrimmedWordP ~ EndTagP ^^
    (e => MyXmlElem(e._1._1._1._1._1._1._2, e._1._1._1._1._1._2, e._1._1._1._2))

  def TrimmedElemP = Trimmed(ElemP)
}