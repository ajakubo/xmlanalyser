package analyser

import analyser.xml.Scanner
import utils.SeqReader
import analyser.xml.MyXmlParser
import analyser.xml.MyXmlElem
import analyser.xml.MyXmlParser.Success
import analyser.xml.MyXmlParser.NoSuccess

package object xml {
  def string2xml(xmlInput: Stream[Char]): MyXmlElem = {
    val xmlTokens = Scanner.tokenStream(xmlInput)
    val xmlReader = new SeqReader(xmlTokens)
    MyXmlParser.TrimmedElemP(xmlReader) match {
      case Success(r, _) => r
      case NoSuccess(msg, next) => throw new Exception("XML: Failed at line %s, column %s: %s".format(
        next.pos.line, next.pos.column, msg))
    }
  }
}